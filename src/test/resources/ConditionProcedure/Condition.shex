PREFIX fhir: <http://hl7.org/fhir/>
PREFIX loinc: <http://loinc.org/rdf#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX sct: <http://snomed.info/id/>
PREFIX v2: <http://hl7.org/fhir/v2/>
PREFIX v3: <http://hl7.org/fhir/v3/>
PREFIX w5: <http://hl7.org/fhir/w5#>
PREFIX xml: <http://www.w3.org/XML/1998/namespace>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

<#MotionSicknessShape> @<#ConditionShape> AND {
  fhir:Condition.code EXTRA fhir:CodeableConcept.coding {
    fhir:CodeableConcept.coding {
      fhir:Coding.code { fhir:value ["37031009"] } ;
      fhir:Coding.system { fhir:value ["http://snomed.info/sct"] } ;
    }
  }
}

<#ConditionShape> {
  fhir:nodeRole [fhir:treeRoot] ;
  fhir:Condition.code @<#SNOMEDCodeableConcept> ;
  fhir:Condition.onsetDateTime { fhir:value xsd:dateTime } ;
  fhir:Condition.subject @<#SubjectRefShape> ;
  fhir:Condition.verificationStatus { fhir:value ["confirmed" "denied"] } ;
  fhir:Resource.id { fhir:value xsd:string }
}

<#SubjectRefShape> {
  fhir:link IRI ;
  fhir:Reference.reference { fhir:value xsd:string }
}

<#SNOMEDCodeableConcept> {
  fhir:CodeableConcept.coding @<#Coding> AND {
    fhir:Coding.system { fhir:value ["http://snomed.info/sct"] } ;
  } ;
  fhir:CodeableConcept.coding @<#Coding>+
}

<#CodeableConcept> {
  fhir:CodeableConcept.coding @<#Coding>+
}

<#Coding> {
  a IRI? ;
  fhir:index xsd:integer? ;
  fhir:Coding.code { fhir:value xsd:string } ;
  fhir:Coding.display { fhir:value xsd:string }? ;
  fhir:Coding.system { fhir:value xsd:string } ;
  fhir:Coding.version { fhir:value xsd:string }?
}

<#Patient> {
  fhir:nodeRole [fhir:treeRoot] ;
  fhir:Patient.active { fhir:value [ true false ] } ;
  fhir:Patient.birthDate { fhir:value xsd:date } ;
  fhir:Patient.gender { fhir:value ["male" "female"] } ;
  fhir:Patient.identifier @<#PatientId> AND {
    fhir:Identifier.system { fhir:value ["cora.patient.id"] } ;
  } ;
  fhir:Patient.identifier @<#PatientId>*,
  fhir:Patient.name {
    fhir:index xsd:integer? ;
    fhir:HumanName.family { fhir:value xsd:string } ;
    fhir:HumanName.given { fhir:value xsd:string } ;
  } ;
  fhir:Resource.id { fhir:value xsd:string }
}

<#PatientId> {
  fhir:index xsd:integer? ;
  fhir:Identifier.system { fhir:value xsd:string } ;
  fhir:Identifier.value { fhir:value xsd:string }
}

